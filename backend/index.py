import json
from flask import Flask, request, abort
from flask_cors import CORS, cross_origin

import ast

from telementry import Telementry

telementry = Telementry()


app = Flask(__name__)
cors = CORS(app)

app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy   dog'
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/telementry/getAllservices', methods=['GET'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def allAvaliableMachines():
    return telementry.allAvaliableMachines()


@app.route('/telementry/findServices', methods=['GET'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def findServices():
    service_id = request.args.get('service_id')
    results = telementry.findServices(service_id)
    if results == False:
        abort(404)
    return results


@app.route('/telementry/addService', methods=['POST'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def addService():
    data = json.loads(request.data)
    return telementry.addService(data['service_id'], data['weights'])


@app.route('/telementry/requestTimeOnMachines', methods=['POST'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def requestTimeOnMachines():
    data = json.loads(request.data)
    return telementry.requestTimeOnMachines(data['service_id'], data['user_id'])


@app.route('/telementry/startCompute', methods=['POST'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def startCompute():
    data = json.loads(request.data)
    return telementry.startCompute(data['service_id'], data['user_id'], int(data['ETA']))


if __name__ == '__main__':
    app.run(debug=True)

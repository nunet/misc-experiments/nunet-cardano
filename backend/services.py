from random import choices
import time


class Service:

    def __init__(self, service_id, weights):
        self.working = False
        self.service_id = service_id
        self.machineAssignedTo = None
        self.weights = weights
        self.states = [True, False]

    def isMachineAssigned(self, userID):
        return self.machineAssignedTo == userID and userID != None

    def isMachineAvalible(self):
        return not self.working

    def markMachineForCompute(self, assignedTo):
        self.working = True
        self.machineAssignedTo = assignedTo

    def compute(self, ETA):
        time.sleep(ETA)
        self.working = False
        print("STATES: ", self.states, self.weights)
        print(choices(self.states, self.weights))
        return choices(self.states, self.weights)[0]

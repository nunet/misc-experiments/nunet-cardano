from services import Service


class Telementry:

    def __init__(self):
        self.Services = {}

    def allAvaliableMachines(self):

        avaliableServices = []

        for service in self.Services:
            if self.Services[service].isMachineAvalible():
                avaliableServices.append(
                    {"service_id": service, "isMachineAvalible": False, "weights": self.Services[service].weights})

        return avaliableServices

    def findServices(self, service_id):
        if service_id in self.Services:
            return {service_id: self.DHT[service_id]}
        return False

    def addService(self, service_id, weights):
        print(service_id, weights)
        self.Services[service_id] = Service(service_id, weights)
        return {"status": "Service Added"}

    def requestTimeOnMachines(self, service_id, userID):
        if service_id in self.Services and self.Services[service_id].isMachineAvalible():
            service = self.Services[service_id]
            service.markMachineForCompute(userID)
            return {"status": True}

        return {"status": False}

    def startCompute(self, service_id, userID, ETA):
        if self.Services[service_id].isMachineAssigned(userID):
            service = self.Services[service_id]
            if service.compute(ETA):
                return {"status": "Job Successful", "success": True}
            return {"status": "Job Failed", "success": False}

        return {"status": "Machine is assigned to someone else", "success": False}

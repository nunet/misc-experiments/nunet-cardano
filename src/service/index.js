import axios from "axios";

let baseURL = "http://127.0.0.1:5000/";

let telementry = "telementry/";

const getAllservices = async () => {
  let resp = await axios.get(baseURL + telementry + "getAllservices");
  return resp;
};

const findServices = async (service_id) => {
  let resp = await axios.get(baseURL + telementry + "findServices", {
    params: service_id,
  });
  console.log(resp);
};

const addService = async (service_id, weights) => {
  let resp = await axios.post(baseURL + telementry + "addService", {
    service_id,
    weights,
  });
  return resp;
};

const requestTimeOnMachines = async (service_id, user_id) => {
  let resp = await axios.post(baseURL + telementry + "requestTimeOnMachines", {
    service_id,
    user_id,
  });
  return resp;
};

const startCompute = async (service_id, user_id, ETA) => {
  let resp = await axios.post(baseURL + telementry + "startCompute", {
    service_id,
    user_id,
    ETA,
  });
  return resp.data;
};

export {
  getAllservices,
  findServices,
  addService,
  requestTimeOnMachines,
  startCompute,
};

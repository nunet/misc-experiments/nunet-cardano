import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import ListOfServices from "./ListOfServices";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ListOfServices />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

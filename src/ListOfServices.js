import { useState, useEffect } from "react";
import {
  getAllservices,
  addService,
  requestTimeOnMachines,
  startCompute,
} from "./service/index";
import Card from "react-bootstrap/Card";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function AddServicePopUp(props) {
  const [successRate, setSuccess] = useState(0);
  const [service_id, setServiceId] = useState("");

  const calculateWeights = (successRate) => {
    let failueRate = Math.abs(successRate - 100);
    return [successRate / 100, failueRate / 100];
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Service
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Service ID</Form.Label>
            <Form.Control
              type="text"
              placeholder="Service ID"
              onChange={(e) => setServiceId(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Success Rate</Form.Label>
            <Form.Range onChange={(e) => setSuccess(e.target.value)} />{" "}
          </Form.Group>

          <Button
            variant="primary"
            type="submit"
            onClick={() =>
              addService(service_id, calculateWeights(successRate))
            }
          >
            Submit
          </Button>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function RequestTimeOnMachines(props) {
  const [user_id, setUserID] = useState("");
  const [ETA, setETA] = useState(0);

  let service_id = props.service_id;

  const requestAndCompute = async (service_id, user_id, ETA, address) => {
    try {
      let status = await requestTimeOnMachines(service_id, user_id);
      if (status) {
        let status = await startCompute(service_id, user_id, ETA);
        console.log(status);
        alert(status.success ? "Job Done" : "Job Failed");
      } else {
        throw Error("Machine Busy");
      }
    } catch (error) {}
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Service
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Service ID</Form.Label>
            <Form.Control type="text" placeholder={service_id} disabled />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>User ID</Form.Label>
            <Form.Control
              type="text"
              placeholder="User ID"
              onChange={(e) => setUserID(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>ETA</Form.Label>
            <Form.Control
              type="Number"
              placeholder="ETA"
              onChange={(e) => setETA(e.target.value)}
            />
          </Form.Group>

          <Button
            variant="primary"
            onClick={() =>
              requestAndCompute(service_id, user_id, ETA).then(() =>
                props.sendTransaction()
              )
            }
          >
            Submit
          </Button>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function ServiceCard(props) {
  console.log(props);

  return (
    <Card className="text-center">
      <Card.Header>Service</Card.Header>
      <Card.Body>
        <Card.Title>Service ID: {props.service.service_id}</Card.Title>
        <Card.Text>
          Machine Success Rate : {props.service.weights[0] * 100} % <br />
          Machine Failure Rate: {props.service.weights[1] * 100} %
        </Card.Text>
        <Button
          variant="primary"
          onClick={(e) => {
            e.preventDefault();
            props.showModel();
            props.setServiceID(props.service.service_id);
          }}
        >
          Request Time On Machine
        </Button>
      </Card.Body>
      <Card.Footer className="text-muted">2 days ago</Card.Footer>
    </Card>
  );
}

export default function (props) {
  const [services, setServices] = useState([]);
  const [modalShow, setModalShow] = useState(false);
  const [modalReq, setModalReq] = useState(false);
  const [selectedService, setServiceID] = useState("");
  const [wallet, setEnabled] = useState(false);
  const [typhon, setTyphon] = useState(null);
  const getServices = async () => {
    setEnabled((await window.cardano.typhon.isEnabled()).status);
    setTyphon(window.cardano.typhon);
    getAllservices()
      .then((data) => {
        setServices(data.data);
        console.log(data.data);
      })
      .catch(() => {
        setServices([]);
      });
  };

  useEffect(() => {
    getServices();
  }, []);

  const sendTransaction = async () => {
    const paymentResponse = await window.cardano.typhon.paymentTransaction({
      outputs: [
        {
          address:
            "addr_test1qz5dux6v6dv4mw7755rvmpggwsxd2cew9r5myw7u47ezpzl7nhr8qc5v4caxgtj3zj87ewqwdml4tve22u5xgu698kgsr4veye",
          amount: "999978",
        },
      ],
    });

    if (paymentResponse.status) {
      alert("Transaction SuccessFull: " + paymentResponse.data.transactionId);
    } else {
      console.log("paymentResponse: ", paymentResponse);
    }
  };

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>All Avaliable Services</h1>

      <button
        className="btn btn-primary"
        sytle={{ margin: "2em" }}
        onClick={() => window.cardano.typhon.enable()}
        disabled={wallet}
      >
        Connect Wallet
      </button>

      <button
        className="btn btn-primary"
        sytle={{ margin: "2em" }}
        onClick={() => setModalShow(true)}
      >
        Add Service
      </button>

      <AddServicePopUp show={modalShow} onHide={() => setModalShow(false)} />
      <RequestTimeOnMachines
        show={modalReq}
        onHide={() => setModalReq(false)}
        service_id={selectedService}
        sendTransaction={sendTransaction}
      />

      {!!services
        ? services.map((service) => {
            return (
              <ServiceCard
                service={service}
                showModel={() => setModalReq(true)}
                setServiceID={setServiceID}
              />
            );
          })
        : "No Services"}
    </div>
  );
}
